package dao;


import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class EntityMgrFactory {
    static public EntityManagerFactory entityManagerFactory =
            Persistence.createEntityManagerFactory("Fifczanowy");

    public static EntityManagerFactory getEntityMgrFactory(){
        return entityManagerFactory;
    }

}
