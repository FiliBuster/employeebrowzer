package dao;

import java.util.List;


public interface Dao<T , ID>{

    List<T> findAll();

    T makePersistent(T entity);

    T findById(ID id);


}
