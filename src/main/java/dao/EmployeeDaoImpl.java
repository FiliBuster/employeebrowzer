package dao;


import model.EmployeesEntity;
import model.SalariesEntity;
import org.hibernate.Criteria;

import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Set;


public class EmployeeDaoImpl extends DaoImpl<EmployeesEntity, Integer> {


    public EmployeeDaoImpl() {
        super(EmployeesEntity.class);
    }

    public List<SalariesEntity> getSalariesById(int id) {
        List<SalariesEntity> valueToReturn = null;

        try {
            em = EntityMgrFactory.entityManagerFactory.createEntityManager();
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<SalariesEntity> criteria = cb.createQuery(SalariesEntity.class);
            Root<SalariesEntity> i = criteria.from(SalariesEntity.class);
            criteria.select(i).where(cb.equal(i.get("empNo"), id));
            Query query = em.createQuery(criteria);
            valueToReturn = query.getResultList();


        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }


        return valueToReturn;
    }


    public List<EmployeesEntity> getEmployeeByLastName(String lastName) {
        List<EmployeesEntity> valueToReturn = null;

        try {
            em = EntityMgrFactory.entityManagerFactory.createEntityManager();
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<EmployeesEntity> criteria = cb.createQuery(EmployeesEntity.class);
            Root<EmployeesEntity> i = criteria.from(EmployeesEntity.class);
            Query query = em.createQuery(criteria.select(i).where(
                    cb.equal(
                            i.get("lastName"), cb.parameter(
                                    String.class, "lastNameToSearch")))).setParameter("lastNameToSearch", lastName);
            valueToReturn = query.getResultList();


        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }


        return valueToReturn;

    }


    public List<EmployeesEntity> getEmployeeWithLikeLastName(String lastNameWithWildcards) {
        List<EmployeesEntity> valueToReturn = null;

        try {
            em = EntityMgrFactory.entityManagerFactory.createEntityManager();

            CriteriaBuilder cb = em.getCriteriaBuilder();

            CriteriaQuery<EmployeesEntity> criteria = cb.createQuery(EmployeesEntity.class);

            Root<EmployeesEntity> i = criteria.from(EmployeesEntity.class);

            Query query = em.createQuery(criteria.select(i).where(
                    cb.like(i.<String>get("lastName"), cb.parameter(String.class, "stringWithWildCards"))
            )).setParameter("stringWithWildCards", lastNameWithWildcards);

            valueToReturn = query.getResultList();


        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }
        return valueToReturn;
    }


    public List<EmployeesEntity> getEmployeeByFullName(String firstName, String lastName) {
        List<EmployeesEntity> valueToReturn = null;

        try {

            em = EntityMgrFactory.entityManagerFactory.createEntityManager();

            CriteriaBuilder cb = em.getCriteriaBuilder();

            CriteriaQuery<EmployeesEntity> criteria = cb.createQuery(EmployeesEntity.class);

            Root<EmployeesEntity> i = criteria.from(EmployeesEntity.class);

            Predicate predicate = cb.equal(i.<String>get("firstName"), cb.parameter(
                    String.class, "firstNameParameter"));

            predicate = cb.and(cb.equal(
                    i.<String>get("lastName"), cb.parameter(String.class, "lastNameParameter")), predicate);

            Query query = em.createQuery(criteria.select(i).where(predicate)).setParameter(
                    "firstNameParameter", firstName).setParameter("lastNameParameter", lastName);

            valueToReturn = query.getResultList();


        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }
        return valueToReturn;
    }

}
