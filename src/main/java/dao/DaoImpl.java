package dao;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import java.io.Serializable;
import java.util.List;

/**
 * Created by ubuntu on 04.08.16.
 */
public class DaoImpl<T , ID extends Serializable> implements Dao<T , ID> {


    EntityManager em;

    Class<T> entityClass;

     DaoImpl(Class entityClass){
        this.entityClass = entityClass;
    }


    @Override
    public T findById(ID id) {
        T valueToReturn = null;

        try {
            em = EntityMgrFactory.entityManagerFactory.createEntityManager();

          valueToReturn = em.find(entityClass, id, LockModeType.NONE);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }

        return valueToReturn;
    }

    @Override
    public List<T> findAll() {
        return null;
    }

    @Override
    public T makePersistent(T entity) {
        T valueToReturn = null;

        try {
            em = EntityMgrFactory.entityManagerFactory.createEntityManager();

            em.getTransaction().begin();
            valueToReturn = em.merge(entity);
            em.getTransaction().commit();

            System.out.println("Success merging " + entity.getClass().toString() + " with db");

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }

        return valueToReturn;

    }
}
