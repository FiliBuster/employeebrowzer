package consoleService;

import dao.Dao;
import dao.EmployeeDaoImpl;
import model.EmployeesEntity;
import model.SalariesEntity;

import java.util.List;

/**
 * Created by ubuntu on 04.08.16.
 */
public class Service {

    Dao dao;
    EmployeeDaoImpl empDao = new EmployeeDaoImpl();



    public EmployeesEntity getEmployeeById(Integer id){
        return empDao.findById(id);
    }

    public void updateEmployee(EmployeesEntity emp){
        empDao.makePersistent(emp);
    }


    public List<SalariesEntity> getSalariesById(int id){

        return empDao.getSalariesById(id);
    }
    public List<EmployeesEntity> getEmployeeByLastName(String lastName){
        return empDao.getEmployeeByLastName(lastName);
    }


    public List<EmployeesEntity> getEmployeeByLike(String input) {
       return empDao.getEmployeeWithLikeLastName(input);
    }

    public List<EmployeesEntity> getEmployeeByFullName(String firstName, String lastName) {
        return empDao.getEmployeeByFullName(firstName, lastName);
    }


}
