package consoleService;

import consoleGui.optionChooser.OptionChooser;
import consoleGui.optionChooser.OptionChooserImpl;
import model.EmployeesEntity;
import model.SalariesEntity;

import java.util.Collection;
import java.util.List;

/**
 * Created by ubuntu on 04.08.16.
 */
public class ConsoleService {

    static ConsoleService instance;

    Service service = new Service();
    OptionChooser optionChooser = new OptionChooserImpl();


    private ConsoleService() {
    }

    public EmployeesEntity findEmployeeById() {
        System.out.println("Enter id of employee:");

        int id = optionChooser.chooseOption(Integer.MAX_VALUE);

        EmployeesEntity employee = service.getEmployeeById(id);

        System.out.println("employee:");

        listRow(employee);

        return employee;
    }

    public List<SalariesEntity> getSalariesById() {
        List<SalariesEntity> salariesList = null;

        System.out.println("Enter id of employee for which you want salaries");

        int id = optionChooser.chooseOption(Integer.MAX_VALUE);

        salariesList = service.getSalariesById(id);

        for (SalariesEntity e : salariesList) {

            System.out.println(e);
        }
        return salariesList;
    }

    public List<EmployeesEntity> getEmployeeByLastName() {
        List<EmployeesEntity> employeesEntityList = null;

        System.out.println("Enter last name of employee to find:");

        String lastName = optionChooser.inputString();

        employeesEntityList = service.getEmployeeByLastName(lastName);

        for (EmployeesEntity e : employeesEntityList) {
            System.out.println(e);
        }
        return employeesEntityList;
    }


    public List<EmployeesEntity> getEmployeeByLikeLatName(){
        List<EmployeesEntity> employeesEntityList = null;

        System.out.println("Enter last name of employee with wild cards % and _");

        String input = optionChooser.inputString();

        employeesEntityList = service.getEmployeeByLike(input);

        for (EmployeesEntity e : employeesEntityList) {
            System.out.println(e);
        }
        return  employeesEntityList;
    }


    public List<EmployeesEntity> getEmployeeByFullName() {
        List<EmployeesEntity> employeesEntityList = null;

        System.out.println("Enter first name");

        String firstName = optionChooser.inputString();

        System.out.println("Enter last name");

        String lastName = optionChooser.inputString();

        employeesEntityList = service.getEmployeeByFullName(firstName, lastName);

        for (EmployeesEntity e : employeesEntityList) {
            System.out.println(e);
        }
        return employeesEntityList;

    }


    public void udateEmploee(EmployeesEntity emp) {
        service.updateEmployee(emp);
    }


    private void listRows(Collection<?> rows) {
        System.out.println(rows);
    }

    private <E> void listRow(E row) {
        System.out.println(row);

    }


    public static ConsoleService getInstance() {
        if (instance == null) {
            instance = new ConsoleService();
        }

        return instance;

    }


}
