package consoleGui.optionChooser;

import java.util.Scanner;

/**
 * Created by ubuntu on 04.08.16.
 */
public class OptionChooserImpl implements OptionChooser {
    static Scanner scaner = new Scanner(System.in);

    @Override
    public int chooseOption(int max) {

        Integer i = null;

        try {

            do {
                i = scaner.nextInt();
                if (i > max || i < 0) System.out.println("Reenter choice, must be between 1 and " + max);;

            }while(i> max || i < 1);
            scaner.nextLine();

        }catch (Exception e){
            e.printStackTrace();
            System.exit(1);
        }

        return i;

    }

    public String inputString(){

        String inputString = null;
        try {

            inputString = scaner.nextLine();


        }catch (Exception e){
            e.printStackTrace();
            System.exit(1);
        }

        return inputString;
    }
}
