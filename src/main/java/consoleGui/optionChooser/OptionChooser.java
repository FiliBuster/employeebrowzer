package consoleGui.optionChooser;

/**
 * Created by ubuntu on 04.08.16.
 */
public interface OptionChooser {

    int chooseOption(int max);
    String inputString();
}
