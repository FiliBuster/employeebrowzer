package consoleGui.menuChoices;

import consoleGui.Console;
import consoleGui.Menus.OrganicMenu;
import consoleGui.headers.HeaderEmployee;
import consoleGui.headers.HeaderInfoEmployee;
import consoleGui.headers.HeaderWhatChange;
import consoleService.ConsoleService;

/**
 * Created by ubuntu on 05.08.16.
 */
public class MenuEmpolyeeChoices implements  MenuChoices {
    ConsoleService consoleService = ConsoleService.getInstance();


    @Override
    public void invokeChoice(int choice) {


        switch (choice) {
            case 1:
                new OrganicMenu(new HeaderInfoEmployee(), new MenuViewInfoEmployee()).viewMenu();
                break;
            case 2:
                new OrganicMenu(new HeaderWhatChange(), new UpdateEmployeeChoices()).viewMenu();
                break;
        }
    }
}
