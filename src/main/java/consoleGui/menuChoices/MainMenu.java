package consoleGui.menuChoices;

import consoleGui.Menus.OrganicMenu;
import consoleGui.headers.HeaderEmployee;
import consoleGui.headers.HeaderWhatChange;
import consoleService.ConsoleService;


public class MainMenu implements MenuChoices {


    ConsoleService consoleService = ConsoleService.getInstance();


    @Override
        public void invokeChoice(int choice) {


            switch (choice){
                case 1 :
                    new OrganicMenu(new HeaderEmployee(), new MenuEmpolyeeChoices()).viewMenu();
                    break;
                case 2:
                  System.exit(0);

            }

    }
}
