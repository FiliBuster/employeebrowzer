package consoleGui.menuChoices;

import consoleService.EmployeePropChanger;

/**
 * Created by ubuntu on 04.08.16.
 */
public class UpdateEmployeeChoices implements MenuChoices {

    EmployeePropChanger propChanger = new EmployeePropChanger();


    @Override
    public void invokeChoice(int choice) {

        switch (choice){
            case 1:
                propChanger.changeEmployeerFirstName();
                break;
            case 2:
                propChanger.changeEmployeerSecondName();
                break;
        }
    }
}
