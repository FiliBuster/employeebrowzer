package consoleGui.menuChoices;

import consoleGui.Menus.OrganicMenu;
import consoleGui.headers.HeaderEmployee;
import consoleService.ConsoleService;

/**
 * Created by ubuntu on 05.08.16.
 */
public class MenuViewInfoEmployee implements MenuChoices{

    ConsoleService consoleService = ConsoleService.getInstance();

    @Override
    public void invokeChoice(int choice) {


        switch (choice) {
            case 1:
                consoleService.findEmployeeById();
                break;
            case 2:
                consoleService.getEmployeeByLastName();
                break;
            case 3:
                consoleService.getEmployeeByLikeLatName();
                break;
            case 4:
                consoleService.getEmployeeByFullName();
                break;
            case 5:
                consoleService.getSalariesById();
                break;



        }
    }
}
