package consoleGui.headers;

/**
 * Created by ubuntu on 05.08.16.
 */
public class HeaderInfoEmployee implements Header {

    public int viewHeader(){
        System.out.println("=====================================================================");
        System.out.println("=========================What you want to do?========================");
        System.out.println("1 : view employee by id");
        System.out.println("2 : search employee by last name");
        System.out.println("3 : search employee by wildcards and like statement of lasName");
        System.out.println("4 : search employee by full name ");
        System.out.println("5 : view employee salaries by id");
        return 5;
    }
}
