package consoleGui.headers;

/**
 * Created by ubuntu on 05.08.16.
 */
public class HeaderEmployee implements Header {

    public int viewHeader(){
        System.out.println("=====================================================================");
        System.out.println("=========================What you want to do?========================");
        System.out.println("1 : View info about employee by id");
        System.out.println("2 : Update employee by id");
        return 2;
    }
}
