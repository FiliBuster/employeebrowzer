package consoleGui.headers;

/**
 * Created by ubuntu on 04.08.16.
 */
public class HeaderWhatChange implements Header {

    @Override
    public int viewHeader() {
        System.out.println("=====================================================================");
        System.out.println("=========================What you want to change?=====================");
        System.out.println("1: EmployeeEntity First Name");
        System.out.println("2: EmployeeEntity Last Name");
        return 2;

    }
}
