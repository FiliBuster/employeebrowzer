package consoleGui.Menus;

import consoleGui.headers.*;
import consoleGui.menuChoices.MenuChoices;
import consoleGui.optionChooser.OptionChooser;
import consoleGui.optionChooser.OptionChooserImpl;

/**
 * Created by ubuntu on 04.08.16.
 */
public class AbstractMenu implements Menu {

    Header header;
    MenuChoices menuChoices;
    OptionChooser optionChooser = new OptionChooserImpl();

    public AbstractMenu(Header header, MenuChoices menuChoices) {
        this.header = header;
        this.menuChoices = menuChoices;
    }


    public void viewMenu(){
        menuChoices.invokeChoice(optionChooser.chooseOption(header.viewHeader()));
    }
}
