package model;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by ubuntu on 05.08.16.
 */
@Entity
@Table(name = "salaries", schema = "employees", catalog = "")
@IdClass(SalariesEntityPK.class)
public class SalariesEntity {
    private int empNo;
    private int salary;
    private Date fromDate;
    private Date toDate;
    private EmployeesEntity employeesByEmpNo;

    @Id
    @Column(name = "emp_no", nullable = false)
    public int getEmpNo() {
        return empNo;
    }

    public void setEmpNo(int empNo) {
        this.empNo = empNo;
    }

    @Basic
    @Column(name = "salary", nullable = false)
    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Id
    @Column(name = "from_date", nullable = false)
    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Basic
    @Column(name = "to_date", nullable = false)
    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SalariesEntity that = (SalariesEntity) o;

        if (empNo != that.empNo) return false;
        if (salary != that.salary) return false;
        if (fromDate != null ? !fromDate.equals(that.fromDate) : that.fromDate != null) return false;
        if (toDate != null ? !toDate.equals(that.toDate) : that.toDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = empNo;
        result = 31 * result + salary;
        result = 31 * result + (fromDate != null ? fromDate.hashCode() : 0);
        result = 31 * result + (toDate != null ? toDate.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "emp_no", referencedColumnName = "emp_no", nullable = false)
    public EmployeesEntity getEmployeesByEmpNo() {
        return employeesByEmpNo;
    }

    public void setEmployeesByEmpNo(EmployeesEntity employeesByEmpNo) {
        this.employeesByEmpNo = employeesByEmpNo;
    }


    @Override
    public String toString() {
        return "SalariesEntity{" +
                "salary=" + salary +
                ", fromDate=" + fromDate +
                ", toDate=" + toDate;
    }
}

